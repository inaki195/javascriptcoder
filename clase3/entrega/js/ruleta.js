/**
 * este programa  simula una ruleta de casino  en la que se genera un numéro aleatorio
 * el programa valida que el número este  entre el rango valido.
 * el usuario después de cada turno  podra retirarse del juego si lo desea o sin se queda sin dinero
 * 
 * 
 */


let min=0
let max=34
dinero=100
costo=20
arruinado=false

let numRandom=min + Math.floor(Math.random()*max)
do{
    numRandom=min + Math.floor(Math.random()*max)
    correcto=false
    //validacion de números
    while(!correcto){
    numElegido=parseInt(prompt("a que numero quieres apostar (numero entre 0 y 34)"))
        if(numElegido>=0 && numElegido<=34)
            correcto=true
    }
    
    if(numElegido !=numRandom){
        dinero=dinero-costo
    }else{
        ganancia=costo*35
        dinero=dinero+ganancia
    }
     if(dinero<20){
            arruinado=true
            break;
        }
    alert(`ha tocado el numero  : ${numRandom} te quedan ${dinero} $`)
    eleccion=prompt("quieres seguir aspostando (di no para dejar de jugar)").toLowerCase()
}while(eleccion!=="no")
    if(arruinado){
        alert(`el juego  arruina muchas familias ,ahora tienes ${dinero}$`)
    }else{
        alert(`ahora tienes el capital de ${dinero} $`)
    }
