
/**
 * este programa  simula una ruleta de casino  en la que se genera un numéro aleatorio
 * el programa valida que el número este  entre el rango valido.
 * el usuario después de cada turno  podra retirarse del juego si lo desea o sin se queda sin dinero
 * mismo programa con funciones
 * 
 */
let eladio = new Jugador("Eladio", 16, 20);
let juanito = new Jugador("juan", 22, 100);
let player = new Jugador("", 0, 0)
let historialNum = []
let historialBet = []
let pares = []
let impares = []
//let zone_0 = [12, 35, 32, 26, 0]
let zone_0 = [1]
let zone_tercio = [33, 16, 24, 5, 10, 32, 8, 30, 11, 36, 13, 27]
let zone_huerfano = [9, 31, 14, 20, 1]
let zone_huerfano2 = [17, 34, 6]
let acertNum = 36
let min = 0
let max = 36
let costo = 17
let arruinado = false
let numElegido = 44
let moneyBet
let acierto = 0
let apostado = false //esta variable comprobada que has apostado algo  mientras
let numRandom = (min, max) => min + Math.floor(Math.random() * max)
//booleanos que comprueban la apuesta
let betNum = false
let betPar = false
let betzone0 = false
let betcolor=false
let coincide = null
let resultado = 111
let colorElegido=""
let number=0
aceptBet=true 

function crearJugador() {
   buildPlayer();
}
function cargarJugador() {
    loadStorageData();
    player = obtenerJugador();
}


function lanzarNumero() {
    numRandom = min + Math.floor(Math.random() * max)
    resultado = numRandom
    let correcto = false
    historialNum.push(resultado)
    Swal.fire(
        'ha tocado el numero '+resultado
      )


}
async function pedirNumero() {
    swal.fire("tenes" + player.getDinero())
    let correcto = false
    betNum = true
    //while (!correcto) {}
        numElegido = await escogerNumero("a que numero quieres apostar (numero entre 0 y 36)",1)
        correcto = validacion(min, max, numElegido);
    historialBet.push(moneyBet)
}
async function betZona0() {
    betzone0 = true
    moneyBet = 0
    moneyBet = await betMoney("cantidad que quieres apostar tienes " + player.getDinero() + "$",1)
    historialBet.push(moneyBet)
}
function alertar(){
    Swal.fire({
        icon: 'error',
        title: ' ',
        text: 'Estas apostando más dinero del que tienes',
      })
}
function comprobarMoney(money){
    money = parseInt(money)
    let wallet=parseInt(getMoneyLocal())
    if(money>wallet){
        return false
    }else{
        return true
    }
    
}
async function betParesImpares() {
    betPar = true;
    moneyBet = 0

    moneyBet = await betMoney("cantidad que quieres apostar tienes " + player.getDinero() + "$",2)
    historialBet.push(moneyBet)
    parImpar = await decisionParImpar()
    console.log(parImpar)
    coincide = null

}
async function betColor() {
    betcolor=true;
    moneyBet = 0
    moneyBet =await betMoney("cantidad que quieres apostar tienes " + player.getDinero() + "$")
    historialBet.push(moneyBet)
     colorElegido= await escogerColor();
}












