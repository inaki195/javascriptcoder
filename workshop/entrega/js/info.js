const listNumber=[  {
    "color":"green",
    "valor":"0",
    "space":" "
},
{
    "color":"red",
    "valor":"1",
    "space":" "
},
{
    "color":"black",
    "valor":"2",
    "space":" "
},
{
    "color":"red",
    "valor":"3",
    "space":" "
},
{
    "color":"black",
    "valor":"4",
    "space":" "
},
{
    "color":"red",
    "valor":"5",
    "space":" "
},
{
    "color":"black",
    "valor":"6",
    "space":" "
},
{
    "color":"red",
    "valor":"7",
    "space":" "
},
{
    "color":"black",
    "valor":"8",
    "space":" "
},
{
    "color":"red",
    "valor":"9",
    "space":" "
},
{
    "color":"black",
    "valor":"10",
    "space":" "
},
{
    "color":"black",
    "valor":"11",
    "space":" "
},
{
    "color":"red",
    "valor":"12",
    "space":" "
},
{
    "color":"black",
    "valor":"13",
    "space":" "
},
{
    "color":"red",
    "valor":"14",
    "space":" "
},
{
    "color":"black",
    "valor":"15",
    "space":" "
},
{
    "color":"red",
    "valor":"16",
    "space":" "
},
{
    "color":"black",
    "valor":"17",
    "space":" "
},
{
    "color":"red",
    "valor":"18",
    "space":" "
},
{
    "color":"red",
    "valor":"19",
    "space":" "
},
{
    "color":"black",
    "valor":"20",
    "space":" "
},
{
    "color":"red",
    "valor":"21",
    "space":" "
},
{
    "color":"black",
    "valor":"22",
    "space":" "
},
{
    "color":"black",
    "valor":"23",
    "space":" "
},
{
    "color":"black",
    "valor":"24",
    "space":" "
},
{
    "color":"red",
    "valor":"25",
    "space":" "
},
{
    "color":"black",
    "valor":"26",
    "space":" "
},
{
    "color":"red",
    "valor":"27",
    "space":" "
},
{
    "color":"black",
    "valor":"28",
    "space":" "
},
{
    "color":"black",
    "valor":"29",
    "space":" "
},
{
    "color":"red",
    "valor":"30",
    "space":" "
},
{
    "color":"black",
    "valor":"31",
    "space":" "
},
{
    "color":"red",
    "valor":"32",
    "space":" "
},
{
    "color":"black",
    "valor":"33",
    "space":" "
},
{
    "color":"red",
    "valor":"34",
    "space":" "
},
{
    "color":"black",
    "valor":"35",
    "space":" "
},
{
    "color":"red",
    "valor":"36",
    "space":" "
}  ]

/**
 * te devuelve el color del numero que ha tocado
 * @param {numero} numero que toca 
 * @returns 
 */
function exportarDatos(num){
    index=num;
    if(index<0)
        index=0;
    const esJson=JSON.stringify(listNumber)
    localStorage.setItem('info',esJson) // convierto a JSON
    const numeros=localStorage.getItem('info') //string
   let  array=JSON.parse(numeros) //
   let elegido= array[index]
    elegido= JSON.stringify(elegido) 
    let elegidosplit=elegido.split(",")
    let color=elegidosplit[0]
    let textomodi=color.slice(10)
    textomodi=textomodi.replace('"','');
    return textomodi;
  
       
}