
 const PorcentajeAciertos=(inte, aciertos)=> {
    let porcen = (aciertos * 100) / inte;
    if (porcen == Infinity)
        porcen = 0;
    return porcen;
}
 
 let playerld=new Jugador("",0,0);
function crearAlerta(msg, dinero) {
    alert(msg + " " + dinero + "$")
}


 
/**
 * 
 * @returns retona el valor maximo del dinero apostado
 */
function maximaApuesta() {
    let max = 0;
    for (let num of historialBet) {
        if (max < num)
            max = num;
    }
    return max;
}
/**
 * metodo que aplica filtro
 */
function obtenerParesImpares() {
    pares = historialNum.filter(item => item % 2 === 0);
    impares = historialNum.filter(item => item % 2 === 1);
}



//funcion de order superior
function mayorPares(pares) {
    return (im) => pares > impares.length

}


async function escogerNumero(msg,opcion) {
    let num = 0;
    let decision=""
    await swal
    .fire ({
        title: msg,
        input: "text" ,
        showCancelButton: true,
        confirmButtonText: "Guardar",
        cancelButtonText: "Cancelar",
    })
    .then(resultado => {
        if (resultado.value) {
            if(opcion==1){
             
        decision = resultado.value;
            }
            if(opcion==2){
            decision=resultado.value;
            return decision;
            }
        }

    });
    num=parseInt(decision)
    return num;
}

function typeBet() {
    let eleccion = 0
    let siguiente = false
    while (!siguiente) {
        eleccion = parseInt(prompt("si quieres apostar a numero pulse 1 si quiere apostar a par o impar pulse 2"))
        siguiente = validacion(1, 2, eleccion)
    }
    return eleccion
}
function validacion(min, max, num) {
    if (num >= min && num <= max) 
        return true;
    
    else {
        return false;
    }
}

function win(opcion, numRam) {
    if (opcion == 1) {
        if (numElegido != numRam) {

            return false

        } else {

            return true;
        }
    }


}
async function decisionParImpar() {
    let opcion =""
    correcto = false
    /*
    while (!correcto) {
        opcion = prompt("par o impar")
        if (opcion == "par" || opcion == "impar") {
            correcto = true;
        }
    }
    */
    await swal
    .fire({
        title: "par o impar",
        input: "text",
        showCancelButton: true,
        confirmButtonText: "Guardar",
        cancelButtonText: "Cancelar",
    })
    .then(resultado => {
        if (resultado.value) {
           opcion=resultado.value;
        }

    });
    return opcion
}
 async function betMoney(msg){
    let cantidad=0
   await swal
    .fire({
        title: msg,
        input: "text",
        showCancelButton: true,
        confirmButtonText: "Guardar",
        cancelButtonText: "Cancelar",
    })
    .then(resultado => {
        if (resultado.value) {
           cantidad=parseInt(resultado.value);
        }

    });
   return cantidad;
}
async function escogerColor(){
    let colorElegido=""
    correcto = false
    /*
    while (!correcto) {
        colorElegido = prompt("red or black or green")
        if (colorElegido == "red" || colorElegido == "black" || colorElegido == "green" ) {
            correcto = true;
        }
    }
    */
    await swal
    .fire({
        title: "red ,black or green",
        input: "text",
        showCancelButton: true,
        confirmButtonText: "Guardar",
        cancelButtonText: "Cancelar",
    })
    .then(resultado => {
        if (resultado.value) {
            colorElegido = resultado.value;
        }
    });
    return colorElegido;


}
function addHistorial(numRam,apostado){
 var body = document.getElementsByTagName("body")[0];
 var tabla = document.getElementById("tabla");
 var tblBody = document.createElement("tbody");
 let type=""

     
     var hilera = document.createElement("tr");
         var celda1 = document.createElement("td");
         var text = document.createTextNode(`${numRam}`);
         addStyle(celda1,numRam);
         
         celda1.appendChild(text);
         hilera.appendChild(celda1);
         var celda3 = document.createElement("td");
         var text3 = document.createTextNode(`${apostado}`);
         celda3.appendChild(text3);
         hilera.appendChild(celda3);
     tblBody.appendChild(hilera);

 tabla.appendChild(tblBody);
 body.appendChild(tabla);
 tabla.setAttribute("border", "2");
    
}
/**
 * metodo que cambia el estilo de las celda con  estilos 
 * @param {} celda 
 * @param {numer} numRam 
 */
function addStyle(celda,numRam){
    celda.style.color = "black";
     color=whoIsColor(numRam);
     if(color=="red"){
        celda.style.backgroundColor="red";
     }
     else if(color=="black"){
        celda.style.backgroundColor="black";
        celda.style.color = "white";
     }
     else if(color=="green"){
        celda.style.backgroundColor="green";
     }
}
 function whoIsColor(num){
    return exportarDatos(num);
    
}
function storageData(nombre,edad,dinero){
    localStorage.setItem('nombre',nombre);
    localStorage.setItem('edad',edad);
    localStorage.setItem('dinero',dinero);
}

async function buildPlayer(){
        let nombre="";
        let edad=0;
        let dinero=0;
        await swal
        .fire ({
            title: "nombre",
            input: "text" ,
            confirmButtonText: "Guardar",
          
        })
        .then(resultado => {
           nombre=resultado.value;
        });
        await swal
        .fire ({
            title: "edad",
            input: "text" ,
            confirmButtonText: "Guardar",
          
        })
        .then(resultado => {
           edad=parseInt(resultado.value);
        });
        await swal
        .fire ({
            title: "dinero",
            input: "text" ,
            confirmButtonText: "Guardar",
          
        })
        .then(resultado => {
            dinero=parseInt(resultado.value);
        });

        storageData(nombre,edad,dinero);

}
function loadStorageData(){
playerld.setNombre(localStorage.getItem('nombre'));
playerld.setEdad(parseInt(localStorage.getItem('edad')));
playerld.setDinero(parseInt(localStorage.getItem('dinero')));
}
function obtenerJugador(){
    return playerld;
}
function updateMoney(money){
    localStorage.setItem('dinero',money);
}
function getMoneyLocal(){
    return localStorage.getItem('dinero')
}
function editAcert(){
    let contenido=document.getElementById('arcertado')
    let porcentaje=PorcentajeAciertos(historialBet.length,acierto)
    contenido.innerHTML=porcentaje+"%";
}
function editMaxBet(){
    let contenido=document.getElementById('maximus')
    let maximus=maximaApuesta();
    contenido.innerHTML=" La maxima apuesta ha sido de "+maximus+"$"
}
