/**
 * metodos donde se comprueba que han ganado el premio y se les devuelve el dinero
 */

function comprobarPar() {
    betPar=comprobarMoney(moneyBet)
    if(betPar){
    coincide = null
    if (parImpar == "impar" && numRandom % 2 == 1) {
        coincide = true;
    } else if (parImpar == "par" && numRandom % 2 == 0) {

        coincide = true;
    } else {
        coincide = false;
    }
    if (!coincide) {
        swal.fire("no hay coincidencia")
        player.perdidas(moneyBet)

    } else {
        player.ganancias(winMoney(moneyBet, 2));
        acierto++;
    }
    swal.fire(`ha tocado el numero  : ${numRandom} te quedan ${player.getDinero()} $`)
    if (moneyBet != 0) {
    addHistorial(numRandom, moneyBet);
    updateMoney(player.getDinero());
    }
    betPar = false;
    updateDom()
    }else{
        alertar();
    }
}
function comprobarZona0() {
    betzone0 = comprobarMoney(moneyBet)
    if (betzone0 == false) {
        alertar()

    } else {
        if (zone_0.includes(numRandom)) {
            player.ganancias(winMoney(moneyBet, 35));
            acierto++;
        } else {
            swal.fire("no hay coincidencia")
            player.perdidas(moneyBet)
        }
        if (moneyBet != 0) {
        addHistorial(numRandom, moneyBet);
        updateMoney(player.getDinero());
        updateDom()
        }
        betzone0 = false;
    }
}
function comprobarNum() {
    betNum=comprobarMoney(moneyBet)
    if (betNum) {
 
        if (!win(1, resultado)) {
            console.log("toco")
            
        }
        if(numElegido!=numRandom){
            player.perdidas(costo)
        console.log("perdiste")
            }
            else if(numElegido==numRandom) {
                console.log("premio")
                player.ganancias(winMoney(costo, 36))
                acierto++;
            }
       
        swal.fire(`ha tocado el numero  : ${numRandom} te quedan ${player.getDinero()} $`)
        historialBet.push(costo)
        if (player.getDinero() < 20) {
            arruinado = true
        }
        if (numElegido!=44) {
        addHistorial(numRandom, costo);
        betNum = false;
        updateMoney(player.getDinero());
        updateDom()
        }
    }else{
        alertar()
    }
   
}
function comprobarColor() {
    betcolor = comprobarMoney(moneyBet)
    if (betcolor == false) {
        alertar()
    }
    else {
        if (colorElegido == whoIsColor(numRandom)) {
            swal.fire("ha tocado")
            player.ganancias(winMoney(moneyBet, 2))
            acierto++;
        } else {
            console.log(colorElegido.length)
            player.perdidas(moneyBet)
            swal.fire("no hay coincidencia")
        }
        if (moneyBet != 0) {
            addHistorial(numRandom, moneyBet);
            updateMoney(player.getDinero());
            updateDom()
            betcolor = false;
        }
    }

}
function updateDom(){
    editAcert();
    editMaxBet();
}

const winMoney = (money, num) => { return money * num; }

