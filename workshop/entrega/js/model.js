jugadores=[]
function recogerDatos(){
let nombre=document.getElementById("nombre").value;
let edad=document.getElementById("edad").value;
let dinero=document.getElementById("dinero").value;
    let newPlayer=new Jugador(nombre,edad,dinero)
    jugadores.push(newPlayer);
    comprobarDatos(nombre,dinero,edad)
    if(valido){
    addDom(newPlayer);
    }
   limpiarCampos();

}
function comprobarDatos(nombre,dinero,edad) {
    valido=true;
    if(nombre.length<=1 || dinero<1 || edad<1) {
        valido=false;
        alert("rellena campos")
    }
    
    return valido;
}

function addDom(player){
    var body = document.getElementsByTagName("body")[0];
    var tabla = document.getElementById("tabla");
    var tblBody = document.createElement("tbody");
     var hilera = document.createElement("tr");
         var celda1 = document.createElement("td");
         var text = document.createTextNode(`${player.getNombre()}`);
         celda1.appendChild(text);
         hilera.appendChild(celda1);
         var celda2 = document.createElement("td");
         var text2 = document.createTextNode(`${player.getEdad()}`);
         celda2.appendChild(text2);
         hilera.appendChild(celda2);
         var celda3 = document.createElement("td");
         var text3 = document.createTextNode(`${player.getDinero()}`);
         celda3.appendChild(text3);
         hilera.appendChild(celda3);
     tblBody.appendChild(hilera);

 tabla.appendChild(tblBody);
 body.appendChild(tabla);
 tabla.setAttribute("border", "2");
}
function pressKeyIntro(){
    if (event.keyCode==13){
        recogerDatos();  
        limpiarCampos();
    }

}
function limpiarCampos(){
    formulario.reset();
}