/**
 * programa que se encargar de dar un nuevo valor a NumRandom
 */

const startMinutes=0.34;
let time=startMinutes*60;
let tocado=false;
let aceptado=false;
const countdown=document.getElementById('countdown');
setInterval(updateCountdown,1000);
function updateCountdown(){
    const minutes=Math.floor((time)/60);
    let seconds=time%60;
    let bonito=seconds.toFixed();
    countdown.innerHTML=` ${bonito}`;
    time--;
    if (seconds<0.1){
        time=20;
     
    }
    if(seconds<0.2){
        //generamos el numero aleatorio
        lanzarNumero();
        if(betNum)
        comprobarNum();
        if(betPar)
        comprobarPar();
        if(betzone0)
        comprobarZona0();
        if(betcolor)
        comprobarColor();
    }
}