
/**
 * este programa  simula una ruleta de casino  en la que se genera un numéro aleatorio
 * el programa valida que el número este  entre el rango valido.
 * el usuario después de cada turno  podra retirarse del juego si lo desea o sin se queda sin dinero
 * mismo programa con funciones
 * 
 */
let eladio = new Jugador("Eladio", 16, 20);
let juanito = new Jugador("juan", 22, 100);
let historialNum = []
let historialBet = []
let pares = []
let impares = []
let zone_0 = [12, 35, 32, 26, 0]
let zone_tercio = [33, 16, 24, 5, 10, 32, 8, 30, 11, 36, 13, 27]
let zone_huerfano = [9, 31, 14, 20, 1]
let zone_huerfano2 = [17, 34, 6]
let acertNum = 36
let min = 0
let max = 35
let costo = 17
let arruinado = false
let numElegido = 44
let moneyBet
let acierto = 0
let numRandom = (min, max) => min + Math.floor(Math.random() * max)
do {
    numRandom = min + Math.floor(Math.random() * max)
    resultado = numRandom

    let correcto = false
    let tipo = typeBet()
    historialNum.push(numRandom)



    if (tipo == 1) {
        alert("tenes" + juanito.getDinero())
        while (!correcto) {

            numElegido = escogerNumero("a que numero quieres apostar (numero entre 0 y 34)")
            correcto = validacion(min, max, numElegido);

        }
        if (!win(1, numRandom)) {
            juanito.perdidas(costo)
            alert("has perdido - " + costo)
        }
        else {
            ganancia = costo * acertNum
            juanito.ganancias(ganancia);
            acierto = acierto + 1;
        }
        alert(`ha tocado el numero  : ${numRandom} te quedan ${juanito.getDinero()} $`)
        historialBet.push(costo)
        if (juanito.getDinero() < 20) {
            arruinado = true
            break;
        }
        addHistorial(numRandom,tipo,costo);
    }
    if (tipo == 2) {
        moneyBet = 0

        moneyBet = escogerNumero("cantidad que quieres apostar tienes " + juanito.getDinero() + "$")
        historialBet.push(moneyBet)
        parImpar = decisionParImpar()
        coincide = null
        if (parImpar == "impar" && numRandom % 2 == 1) {
            coincide = true;
        } else if (parImpar == "par" && numRandom % 2 == 0) {

            coincide = true;
        } else {
            coincide = false;
        }


        if (!coincide) {
            alert("no hay coincidencia")
            juanito.perdidas(moneyBet)

        } else {
            ganancia = moneyBet * 2
            juanito.ganancias(ganancia)
            acierto = acierto + 1;
        }
        alert(`ha tocado el numero  : ${numRandom} te quedan ${juanito.getDinero()} $`)
        addHistorial(numRandom,tipo,moneyBet);
    }
    if (juanito.getDinero() < 1)
        break
    eleccion = prompt("quieres seguir aspostando (di no para dejar de jugar)").toLowerCase()
} while (eleccion !== "no")
obtenerParesImpares()
veredicto(arruinado)

function paresImpares(){
    moneyBet = 0

    moneyBet = escogerNumero("cantidad que quieres apostar tienes " + juanito.getDinero() + "$")
    historialBet.push(moneyBet)
    parImpar = decisionParImpar()
    coincide = null
    if (parImpar == "impar" && numRandom % 2 == 1) {


        coincide = true;
    } else if (parImpar == "par" && numRandom % 2 == 0) {

        coincide = true;
    } else {
        coincide = false;
    }


    if (!coincide) {
        alert("no hay coincidencia")
        juanito.perdidas(moneyBet)

    } else {
        ganancia = moneyBet * 2
        juanito.ganancias(ganancia)
        acierto = acierto + 1;
    }
    alert(`ha tocado el numero  : ${numRandom} te quedan ${juanito.getDinero()} $`)
    addHistorial(numRandom,tipo,moneyBet);

}