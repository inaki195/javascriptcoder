
function crearAlerta(msg, dinero) {
    alert(msg + " " + dinero + "$")
}

/**
 * @param {booleano} arruinado dice si te has arruinado
 */
function veredicto(arruinado) {
    intentos = historialNum.length
    historialNum = historialNum.join('  |  ')
    if (arruinado) {
        crearAlerta("el juego  arruina muchas familias " + juanito.getNombre() + ",ahora tienes", juanito.getDinero())

    } else {
        crearAlerta(juanito.getNombre() + " ahora tienes el capital de ", juanito.getDinero())
    }
    let predominaPares = mayorPares(pares.length)

    alert(`números que han tocado ` + historialNum)
    let empate = false;
    if (pares.length == impares.length)
        empate = true
    if (!empate) {
        if (predominaPares(impares.length)) {
            alert("hay más numeros pares ")
        } else {
            alert("hay más numeros impares ")
        }
    }
    let aciertoP = PorcentajeAciertos(intentos, acierto);
    maxBet = maximaApuesta()
    alert("porcentaje de aciertos " + aciertoP + "% y la apuesta más alta ha sido de " + maxBet)
}
/**
 * 
 * @returns retona el valor maximo del dinero apostado
 */
function maximaApuesta() {
    let max = 0;
    for (let num of historialBet) {
        if (max < num)
            max = num;
    }
    return max;
}
/**
 * metodo que aplica filtro
 */
function obtenerParesImpares() {
    pares = historialNum.filter(item => item % 2 === 0);
    impares = historialNum.filter(item => item % 2 === 1);
}


function PorcentajeAciertos(inte, aciertos) {

    let porcen = (aciertos * 100) / inte;
    if (porcen == Infinity)
        porcen = 0;
    return porcen;
}
//funcion de order superior
function mayorPares(pares) {
    return (im) => pares > impares.length

}


function escogerNumero(msg) {
    num = parseInt(prompt(msg))
    return num
}

function typeBet() {
    let eleccion = 0
    let siguiente = false
    while (!siguiente) {
        eleccion = parseInt(prompt("si quieres apostar a numero pulse 1 si quiere apostar a par o impar pulse 2"))
        siguiente = validacion(1, 2, eleccion)
    }
    return eleccion
}
function validacion(min, max, num) {
    if (num >= min && num <= max) {
        return true;
    }
    else {
        return false;
    }
}

function win(opcion, numRam) {
    if (opcion == 1) {
        if (numElegido != numRam) {

            return false

        } else {

            return true;
        }
    }


}
function decisionParImpar() {
    correcto = false
    while (!correcto) {
        opcion = prompt("par o impar")
        if (opcion == "par" || opcion == "impar") {
            correcto = true;
        }
    }
    return opcion
}
function addHistorial(numRam,apostado){
 var body = document.getElementsByTagName("body")[0];
 var tabla = document.getElementById("tabla");
 var tblBody = document.createElement("tbody");
 let type=""

     
     var hilera = document.createElement("tr");
         var celda1 = document.createElement("td");
         var text = document.createTextNode(`${numRam}`);
         celda1.appendChild(text);
         hilera.appendChild(celda1);
        
         var celda3 = document.createElement("td");
         var text3 = document.createTextNode(`${apostado}`);
         celda3.appendChild(text3);
         hilera.appendChild(celda3);
     tblBody.appendChild(hilera);

 tabla.appendChild(tblBody);
 body.appendChild(tabla);
 tabla.setAttribute("border", "2");
    
}