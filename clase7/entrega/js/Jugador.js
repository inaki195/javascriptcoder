class Jugador {
    constructor(nombre, edad, dinero) {
        this.nombre = nombre;
        this.edad = edad;
        this.dinero = dinero;
    }
    getEdad() {
        return this.edad;
    }
    getNombre() {
        return this.nombre;
    }
    getDinero() {
        return this.dinero;
    }
    setDinero(cantidad) {
        this.dinero = cantidad;
    }
    perdidas(cantidad) {
        this.dinero = this.dinero - cantidad;
    }
    ganancias(cantidad) {
        this.dinero = this.dinero + cantidad;
    }

}