/*
3. Para aprobar la cursada de la materia de algoritmos se requiere de un puntaje superior a 6.50, realizar un programa
 que le pida al usuario ingresar su calificación y devuelva por consola lo siguiente.
Si calificación es mayor a 9 ---> Aprobado, no rinde final
Si calificación esta entre 6.50 y 9 ---> Aprobado, rinde final.
Si calificación es menor a 6.50 ----> Debe presentar recuperatorio. 
*/
let nota=parseFloat(prompt("dame un numero"))
if(nota>=9){
    console.log("Aprobado, no rinde final")
}
else if(nota<9 && nota>=6.5){
    console.log("Aprobado, rinde final") 
}
else if(nota<6.5){
    console.log("Debe presentar recuperatoria")
}
